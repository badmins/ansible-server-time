# NTP

Ansible role to configure Timezones and enable NTP client

## Requirements

Ansible

## Role Variables

```
ntp_timezone: UTC
```

Define server timezone

```
ntp_servers:
    - "0.nl.pool.ntp.org"
    - "1.nl.pool.ntp.org"
    - "2.nl.pool.ntp.org"
    - "3.nl.pool.ntp.org"
```

Define a list of possible NTP servers

```
ntp_restrict_policies:
    - "default ignore"
    - "127.0.0.1"
    - "::1"
```

Define NTP restrictions


## Example Playbook

```
- hosts: db-servers
  roles:
    - { role: server-time }
```

## Test role

```
mkdir tests/roles/
ln -s ../.. tests/roles/server-time
ansible-playbook -i tests/inventory tests/test.yml --syntax-check
```

You can test it on your PC without *--check* but it will **will udpate your timezone and enable NTP**!

```
ansible-playbook -K -i tests/inventory tests/test.yml --check
```

## License

MIT
